/**
 * Copyright 2020 Christian Daigneault-St-Arnaud
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions
 *    and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 *    following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef CONSOLE_BSP_CONSOLE_UART_H_
#define CONSOLE_BSP_CONSOLE_UART_H_

// Includes
#include "console_io.h"
#include "dma.h"
#include "gpio.h"
#include "uart.h"

// FreeRTOS
#include "FreeRTOS.h"
#include "semphr.h"

class Console_io_uart : public Console_io, public Uart{
public:
    Console_io_uart( const gpio::Port    tx_port_
                   , const gpio::Pin     tx_pin_
                   , const gpio::Af      tx_af_
                   , const gpio::Port    rx_port_
                   , const gpio::Pin     rx_pin_
                   , const gpio::Af      rx_af_
                   , const UartX         uartx_
                   , const std::size_t   buffer_size_
                   , const IRQn_Type     irq_
                   , const std::uint32_t priority_
                   , const std::uint32_t sub_priority_
                   , const DmaX          dmax_
#ifdef STM32F4
                   , const dma::StreamX  streamx_
#endif
                   , const dma::ChannelX channelx_
                   , const dma::Priority dma_priority_
                   , std::uint32_t       timeout_ms_ = 10
                   )
    : Uart(uartx_, buffer_size_, irq_, priority_, sub_priority_)
    , tx_gpio(tx_port_, tx_pin_)
    , tx_af(tx_af_)
    , rx_gpio(rx_port_, rx_pin_)
    , rx_af(rx_af_)
    , rx_dma( dmax_
            , streamx_
            , channelx_
            , dma::Direction::periph_to_memory
            , dma::Mode::normal
            , dma_priority_)
    , timeout_ms(timeout_ms_)
    , semaphore(NULL)
    {}

    void open()
    {
        tx_gpio.enable_clock();
        tx_gpio.set_alternate_function( tx_af
                                      , gpio::Out_type::push_pull
                                      , gpio::Out_speed::very_high
                                      , gpio::Pull::no_pull);

        rx_gpio.enable_clock();
        rx_gpio.set_alternate_function( tx_af
                                      , gpio::Out_type::push_pull
                                      , gpio::Out_speed::very_high
                                      , gpio::Pull::pull_up);
        receive_dma(rx_dma);
        Uart::open();
    }

    void close()
    {
        Uart::close();
    }

    int print(const std::string out)
    {
        transmit(out.begin(),out.end(), timeout_ms);
        return out.size();
    }

    int get_char()
    {
        int key;

        if (is_data_available()) {
            key = read();
        } else {
            key = no_key;
        }
        return key;
    }

    void create_lock()
    {
        semaphore = xSemaphoreCreateMutex();

        if( semaphore == NULL )
        {
            /* There was insufficient FreeRTOS heap available for the semaphore to
            be created successfully. */
            while(1);
        }
        unlock();
    }

    void unlock()
    {
        xSemaphoreGive(semaphore);
    }

    bool lock()
    {
        return xSemaphoreTake(semaphore, portMAX_DELAY);
    }

private:

    const Gpio      tx_gpio;
    const gpio::Af  tx_af;
    const Gpio      rx_gpio;
    const gpio::Af  rx_af;
    Dma             rx_dma;
    std::uint32_t   timeout_ms;
    // FreeRTOS
    SemaphoreHandle_t semaphore;

    // Constants
    static constexpr int no_key = EOF;
};

#endif /* CONSOLE_BSP_CONSOLE_UART_H_ */
