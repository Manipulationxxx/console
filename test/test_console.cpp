/**
 * Copyright 2020 Christian Daigneault-St-Arnaud
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions
 *    and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 *    following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

// Include test framework, catch2 & fakeit
#include "fakeit.hpp"
using namespace fakeit;

// Test
#include "command_table_for_test.h"
#include "../src/command_parser.h"
#include "../src/command_table.h"
#include "../src/console.h"

using namespace console;

// Mock
/* Had problem with fakeit. Since const char* is used, the verification fail
 * because the string is not saved by fakeit.
 */
class Mock_io : public Console_io {
public:
    Mock_io()
    {
        get_char_index = 0;
        reset();
    }
    int print(const char* log_message) override
    {
        print_history.emplace_back(log_message);

        return 1;
    }

    int get_char() override
    {
        int out;

        if (get_char_index < get_char_val.size()) {
            out = get_char_val[get_char_index]; // NOLINT(cert-str34-c)
            ++get_char_index;
        } else {
            out = get_char_val.back();  // NOLINT(cert-str34-c)
        }

        return out;
    }

    void reset()
    {
        print_history.clear();
        get_char_val.clear();
        get_char_index = 0;
    }

    std::uint32_t get_char_index;
    std::vector<std::string> print_history;
    std::vector<char> get_char_val;
};

TEST_CASE( "Command parser test", "[parser]" ) {

    constexpr std::uint8_t max_input_size = 64;
    constexpr std::uint8_t max_output_size = 16;

    String_factory<max_input_size> input_str;
    String_factory<max_output_size> output_str;

    std::pmr::string& input = input_str.get_string();
    std::pmr::string& output = output_str.get_string();

    Parser parser(input, output);

    SECTION( "Parse one parameter" ) {

        input = "CMD_NAME parameter1";

        parser.get_parameter(1);
        REQUIRE( output == "parameter1" );
    }

    SECTION( "Parse first of three parameters" ) {
        input = "CMD_NAME parameter1 parameter2 parameter3";

        parser.get_parameter(1);
        REQUIRE( output == "parameter1" );
    }

    SECTION( "Parse second of three parameters" ) {
        input = "CMD_NAME parameter1 parameter2 parameter3";

        parser.get_parameter(2);
        REQUIRE( output == "parameter2" );
    }

    SECTION( "Parse third of three parameters" ) {
        input = "CMD_NAME parameter1 parameter2 123";

        parser.get_parameter(3);
        REQUIRE( output == "123" );
    }

    SECTION( "Get first of NO parameter" ) {
        input = "CMD_NAME";

        parser.get_parameter(1);
        REQUIRE( output.empty() );
    }

    SECTION( "Get second of NO parameter" ) {
        input = "CMD_NAME";

        parser.get_parameter(2);
        REQUIRE( output.empty() );
    }

    SECTION( "Parameter start but no parameter" ) {
        input = "CMD_NAME ";

        parser.get_parameter(1);
        REQUIRE( output.empty() );
    }

    SECTION( "Get command name" ) {
        input = "CMD_NAME P1 P2";

        parser.get_name();
        REQUIRE( output == "CMD_NAME" );
    }
}

TEST_CASE( "Command table test", "[table]" ) {

    Mock_io io;
    Command_table table;
    
    constexpr std::uint8_t max_input_size = 64;
    String_factory<max_input_size> input_str;

    std::pmr::string& input = input_str.get_string();

    SECTION( "Mock validation" ) {
        io.print("validation");

        REQUIRE( io.print_history.size() == 1 );
        REQUIRE( io.print_history[0] == "validation");
    }

    SECTION( "Validate table getter" ) {
        const commands_table_t* test_table = t0_cmd::get_t0_table();
        std::string name = test_table[0].name;

        REQUIRE( name == "cmd0");
    }

    SECTION( "Search existing command" ) {
        std::int32_t index;
        table.set_table(t0_cmd::get_t0_table());

        input = "cmd0";
        index = table.search(input);
        REQUIRE( index == 0);

        input = "cmd1";
        index = table.search(input);
        REQUIRE( index == 1);

        input = "cmd3";
        index = table.search(input);
        REQUIRE( index == 3);
        
        input = "cmd2";
        index = table.search(input);
        REQUIRE( index == 2);
    }
    
    SECTION( "Search non existing command" ) {

        std::int32_t index;
        table.set_table(t0_cmd::get_t0_table());

        index = table.search("no_cmd");
        REQUIRE( index == console::not_found);
        
        index = table.search("no_cmd \n");
        REQUIRE( index == console::not_found);
        
        index = table.search("no_cmd parameter1 \r\n");
        REQUIRE( index == console::not_found);
    }
    
    SECTION( "Execute command in base table" ) {
        std::int32_t index = 0;
        input = "";
        
        table.set_table(t0_cmd::get_t0_table());
        table.execute(index, io, input);
        REQUIRE( io.print_history.size() == 1);
        REQUIRE( io.print_history[0] == "cmd0 called");
        
        input = "cmd0 parameter11 \n";
        table.execute(index, io, input);
        REQUIRE( io.print_history.size() == 2);
        REQUIRE( io.print_history[1] == "cmd0 called");
    }

    SECTION( "Execute command in table 1" ) {
        std::int32_t index = 1;
        input = "";
        
        table.set_table(t1_cmd::get_t1_table());
        table.execute(index, io, input);
        REQUIRE( io.print_history.size() == 1);
        REQUIRE( io.print_history[0] == "t1cmd1 called");
    }
    
    SECTION( "Execute command with 1 character in name" ) {
        std::int32_t index = 2;
        input = "";

        table.set_table(t1_cmd::get_t1_table());
        table.execute(index, io, input);
        REQUIRE( io.print_history.size() == 1);
        REQUIRE( io.print_history[0] == "t1 1 called");
    }
    
    SECTION( "Get name of command" ) {
        std::int32_t index = 0;
        std::string  name;
        
        table.set_table(t0_cmd::get_t0_table());
        name = table.get_command_name(index);

        REQUIRE( name == "cmd0" );
    }
    
    SECTION( "Get size of command name" ) {
        std::int32_t index = 0;
        std::int32_t size;

        table.set_table(t0_cmd::get_t0_table());
        size = table.get_command_name_size(index);

        REQUIRE( size == 4 );
    }

    SECTION( "Help command" ) {
        std::int32_t index;
        input = "help";
        
        table.set_table(t1_cmd::get_t1_table());
        index = table.search(input);

        REQUIRE( index == console::help_found );
    }
    
    SECTION( "Get help message of cmd0 (option set to false)" ) {
        std::int32_t index = 0;
        std::string help_msg;
        
        table.set_table(t0_cmd::get_t0_table());
        help_msg = table.get_help_msg(index);

        REQUIRE( help_msg.empty() );
    }
    
    SECTION( "Get help message of table 1 first command (option set to true)" ) {
        std::int32_t index = 0;
        std::string help_msg;
        
        table.set_table(t1_cmd::get_t1_table());
        help_msg = table.get_help_msg(index);

        REQUIRE( help_msg == "t1cmd0 help" );
    }
    
    SECTION( "Quit command" ) {
        std::int32_t index;
        input = "quit";
        
        table.set_table(t1_cmd::get_t1_table());
        index = table.search(input);

        REQUIRE( index == console::quit_found );
    }
}

TEST_CASE( "Console util test", "[util]" ) {
    
    SECTION( "Help enable" ) {
        
        std::string help_msg;
        
        help_msg = help<true>("help msg enabled");
        REQUIRE( help_msg == "help msg enabled" );
    }
    
    SECTION( "Help disabled" ) {
        
        std::string help_msg;
        
        help_msg = help<false>("help msg disabled");
        REQUIRE( help_msg.empty() );
    }
}

// Test helper function prototypes
void run_process(Console& cli, std::uint32_t nb_time);

TEST_CASE( "Console test", "[console]" ) {

    Mock_io io;

    Data<64, 64> cli_data;
    std::pmr::string& cli_buffer = cli_data.get_buffer();
    root_table_t& cli_root_table = cli_data.get_root_table();

    SECTION( "Start" ) {

        io.get_char_val = {EOF};

        Console cli(io, cli_buffer, cli_root_table, true, ">", "\r\n");
        
        cli.start("");

        REQUIRE( io.print_history.size() == 4      );
        REQUIRE( io.print_history[0]     == "\r\n" );
        REQUIRE( io.print_history[1].empty()       );
        REQUIRE( io.print_history[2]     == "\r\n" );
        REQUIRE( io.print_history[3]     == ">"    );
    }


    SECTION( "Test echo disable" ) {

        io.get_char_val = {'c', 'm', 'd', '0', EOF};

        bool echo = false;

        Console cli(io, cli_buffer, cli_root_table, echo, ">", "\r\n");

        cli.start("");

        cli.process();

        REQUIRE( io.get_char_index == 5 );
    }

    SECTION( "Test echo enabled" ) {

        bool echo = true;
        Console cli(io, cli_buffer, cli_root_table, echo, ">", "\r\n");
        cli.start("");

        io.reset();
        io.get_char_val = {'c', 'm', 'd', '0', EOF};

        cli.process();

        REQUIRE( io.get_char_index       == 5 );
        REQUIRE( io.print_history.size() == 4 );
        REQUIRE( io.print_history[0]     == "c" );
        REQUIRE( io.print_history[1]     == "m" );
        REQUIRE( io.print_history[2]     == "d" );
        REQUIRE( io.print_history[3]     == "0" );
    }

    SECTION( "No command table added" ) {

        bool echo = false;
        std::string endline = "\r\n";
        std::string prompt  = ">";

        Console cli(io, cli_buffer, cli_root_table, echo, prompt.c_str(), endline.c_str());

        cli.start("");

        io.reset();
        io.get_char_val = {'n', 'c', 'm', 'd', '\n', EOF};

        cli.process();

        REQUIRE( io.print_history.size() == 4                   );
        REQUIRE( io.print_history[0]     == endline             );
        REQUIRE( io.print_history[1]     == "Command not found" );
        REQUIRE( io.print_history[2]     == endline             );
        REQUIRE( io.print_history[3]     == prompt              );
    }

    SECTION( "Enter valid command" ) {

        bool echo = false;
        std::string endline = "\r\n";
        std::string prompt  = ">";

        Console cli(io, cli_buffer, cli_root_table, echo, prompt.c_str(), endline.c_str());

        cli.add_cmd_table("Base table", t0_cmd::get_t0_table());
        cli.start("");

        io.reset();
        io.get_char_val = {EOF, 'c', EOF, 'm', EOF, EOF, EOF, 'd', '0', EOF, '\r', EOF};

        run_process(cli, 12);

        REQUIRE( io.print_history.size() == 4             );
        REQUIRE( io.print_history[0]     == endline       );
        REQUIRE( io.print_history[1]     == "cmd0 called" );
        REQUIRE( io.print_history[2]     == endline       );
        REQUIRE( io.print_history[3]     == prompt        );
    }

    SECTION( "Enter valid command with parameter" ) {

        bool echo = false;
        std::string endline = "\r\n";
        std::string prompt  = ">";

        Console cli(io, cli_buffer, cli_root_table, echo, prompt.c_str(), endline.c_str());

        cli.add_cmd_table("Base table", t0_cmd::get_t0_table());
        cli.start("");

        io.reset();
        io.get_char_val = {'c', 'm', 'd', '0', ' ', 'p', '1','\r', EOF};

        cli.process();
        
        REQUIRE( io.print_history.size() == 4             );
        REQUIRE( io.print_history[0]     == endline       );
        REQUIRE( io.print_history[1]     == "cmd0 called" );
        REQUIRE( io.print_history[2]     == endline       );
        REQUIRE( io.print_history[3]     == prompt        );
    }

    SECTION( "Enter invalid command" ) {

        bool echo = false;
        std::string endline = "\r\n";
        std::string prompt  = ">";

        Console cli(io, cli_buffer, cli_root_table, echo, prompt.c_str(), endline.c_str());

        cli.add_cmd_table("Base table", t0_cmd::get_t0_table());
        cli.start("");

        io.reset();
        io.get_char_val = {'n', 'c', 'm', 'd', '\n', EOF};

        cli.process();

        REQUIRE( io.print_history.size() == 4                   );
        REQUIRE( io.print_history[0]     == endline             );
        REQUIRE( io.print_history[1]     == "Command not found" );
        REQUIRE( io.print_history[2]     == endline             );
        REQUIRE( io.print_history[3]     == prompt              );
    }

    SECTION( "Command return error" ) {

        bool echo = false;
        std::string endline = "\r\n";
        std::string prompt  = ">";

        Console cli(io, cli_buffer, cli_root_table, echo, prompt.c_str(), endline.c_str());

        cli.add_cmd_table("App1 table", t1_cmd::get_t1_table());
        cli.start("");

        io.reset();
        io.get_char_val = {'t', '1', 'c', 'm', 'd', '1', '\r', EOF};

        cli.process();

        REQUIRE( io.print_history.size() == 7               );
        REQUIRE( io.print_history[0]     == endline         );
        REQUIRE( io.print_history[1]     == "t1cmd1 called" );
        REQUIRE( io.print_history[2]     == endline         );
        REQUIRE( io.print_history[3]     == "Error: "       );
        REQUIRE( io.print_history[4]     == "t1cmd1 help"   );
        REQUIRE( io.print_history[5]     == endline         );
        REQUIRE( io.print_history[6]     == prompt          );
    }

    SECTION( "Enter LF " ) {

        bool echo = true;
        std::string endline = "\r\n";
        std::string prompt  = ">";
        Console cli(io, cli_buffer, cli_root_table, echo, prompt.c_str(), endline.c_str());

        cli.add_cmd_table("Base table", t0_cmd::get_t0_table());
        cli.start("");

        io.reset();
        io.get_char_val = {'\r', '\r', '\r', 'x', EOF};

        run_process(cli, 12);

        REQUIRE( io.print_history.size() == 7               );
        REQUIRE( io.print_history[0]     == endline         );
        REQUIRE( io.print_history[1]     == prompt          );
        REQUIRE( io.print_history[2]     == endline         );
        REQUIRE( io.print_history[3]     == prompt          );
        REQUIRE( io.print_history[4]     == endline         );
        REQUIRE( io.print_history[5]     == prompt          );
        REQUIRE( io.print_history[6]     == "x"             );
    }

    SECTION( "Enter CR " ) {

        bool echo = true;
        std::string endline = "\r\n";
        std::string prompt  = ">";
        Console cli(io, cli_buffer, cli_root_table, echo, prompt.c_str(), endline.c_str());

        cli.add_cmd_table("Base table", t0_cmd::get_t0_table());
        cli.start("");

        io.reset();
        io.get_char_val = {'\n', '\n', '\n', 'x', EOF};

        run_process(cli, 12);

        REQUIRE( io.print_history.size() == 7               );
        REQUIRE( io.print_history[0]     == endline         );
        REQUIRE( io.print_history[1]     == prompt          );
        REQUIRE( io.print_history[2]     == endline         );
        REQUIRE( io.print_history[3]     == prompt          );
        REQUIRE( io.print_history[4]     == endline         );
        REQUIRE( io.print_history[5]     == prompt          );
        REQUIRE( io.print_history[6]     == "x"             );
    }

    SECTION( "Enter CR+LF " ) {

        bool echo = true;
        std::string endline = "\r\n";
        std::string prompt  = ">";
        Console cli(io, cli_buffer, cli_root_table, echo, prompt.c_str(), endline.c_str());

        cli.add_cmd_table("Base table", t0_cmd::get_t0_table());
        cli.start("");

        io.reset();
        io.get_char_val = {'\r', '\n', '\r', '\n', '\r', '\n', 'x', EOF};

        run_process(cli, 12);

        REQUIRE( io.print_history.size() == 7               );
        REQUIRE( io.print_history[0]     == endline         );
        REQUIRE( io.print_history[1]     == prompt          );
        REQUIRE( io.print_history[2]     == endline         );
        REQUIRE( io.print_history[3]     == prompt          );
        REQUIRE( io.print_history[4]     == endline         );
        REQUIRE( io.print_history[5]     == prompt          );
        REQUIRE( io.print_history[6]     == "x"             );
    }

    SECTION( "Delete key" ) {

        bool echo = true;
        std::string endline = "\r\n";
        std::string prompt  = ">";
        Console cli(io, cli_buffer, cli_root_table, echo, prompt.c_str(), endline.c_str());

        cli.add_cmd_table("Base table", t0_cmd::get_t0_table());
        cli.start("");

        io.reset();
        io.get_char_val = {'n', 'c', 'm', 8, 127, 'd', EOF};

        cli.process();

        REQUIRE( io.print_history.size() == 6       );
        REQUIRE( io.print_history[0]     == "n"     );
        REQUIRE( io.print_history[1]     == "c"     );
        REQUIRE( io.print_history[2]     == "m"     );
        REQUIRE( io.print_history[3]     == "\b \b" );
        REQUIRE( io.print_history[4]     == "\b \b" );
        REQUIRE( io.print_history[5]     == "d"     );
    }

    SECTION( "Console commands help") {
        bool echo = false;
        std::string endline = "\r\n";
        std::string prompt  = ">";
        Console cli(io, cli_buffer, cli_root_table, echo, prompt.c_str(), endline.c_str());

        cli.add_cmd_table("App1 table", t1_cmd::get_t1_table());
        cli.start("");

        io.reset();
        io.get_char_val = {'h', 'e', 'l', 'p', '\n', EOF};

        cli.process();

        REQUIRE( io.print_history.size() == 19            );
        REQUIRE( io.print_history[0]     == endline       );
        REQUIRE( io.print_history[1]     == "App1 table"  );
        REQUIRE( io.print_history[2]     == endline       );
        REQUIRE( io.print_history[3]     == "t1cmd0"      );
        REQUIRE( io.print_history[4].empty()              );
        REQUIRE( io.print_history[5]     == " : "         );
        REQUIRE( io.print_history[6]     == "t1cmd0 help" );
        REQUIRE( io.print_history[7]     == endline       );
        REQUIRE( io.print_history[8]     == "t1cmd1"      );
        REQUIRE( io.print_history[9].empty()              );
        REQUIRE( io.print_history[10]    == " : "         );
        REQUIRE( io.print_history[11]    == "t1cmd1 help" );
        REQUIRE( io.print_history[12]    == endline       );
        REQUIRE( io.print_history[13]    == "1"           );
        REQUIRE( io.print_history[14]    == "     "       );
        REQUIRE( io.print_history[15]    == " : "         );
        REQUIRE( io.print_history[16]    == "1 help"      );
        REQUIRE( io.print_history[17]    == endline       );
        REQUIRE( io.print_history[18]    == prompt        );
    }

    SECTION( "Multiple tables added" ) {
        bool echo = false;
        std::string endline = "\r\n";
        std::string prompt  = ">";

        Console cli(io, cli_buffer, cli_root_table, echo, prompt.c_str(), endline.c_str());

        cli.add_cmd_table("T0", t0_cmd::get_t0_table());
        cli.add_cmd_table("T1", t1_cmd::get_t1_table());
        cli.start("");

        // Go to T1
        io.reset();
        io.get_char_val = {'T', '1', '\n', EOF};

        cli.process();

        // Call t1cmd0 command in T1
        io.reset();
        io.get_char_val = {'t', '1', 'c', 'm', 'd', '0', '\n', EOF};

        cli.process();

        REQUIRE( io.print_history.size() == 4               );
        REQUIRE( io.print_history[0]     == endline         );
        REQUIRE( io.print_history[1]     == "t1cmd0 called" );
        REQUIRE( io.print_history[2]     == endline         );
        REQUIRE( io.print_history[3]     == prompt          );

        // Go to T0
        io.reset();
        io.get_char_val = {'T', '0', '\n', EOF};

        cli.process();

        // Call cmd0 command in T0
        io.reset();
        io.get_char_val = {'c', 'm', 'd', '0', '\n', EOF};

        cli.process();

        REQUIRE( io.print_history.size() == 4               );
        REQUIRE( io.print_history[0]     == endline         );
        REQUIRE( io.print_history[1]     == "cmd0 called"   );
        REQUIRE( io.print_history[2]     == endline         );
        REQUIRE( io.print_history[3]     == prompt          );
    }

    SECTION( "Multiple tables added, remove one" ) {
        bool echo = false;
        std::string endline = "\r\n";
        std::string prompt  = ">";

        Console cli(io, cli_buffer, cli_root_table, echo, prompt.c_str(), endline.c_str());

        cli.add_cmd_table("T0", t0_cmd::get_t0_table());
        cli.add_cmd_table("T1", t1_cmd::get_t1_table());
        cli.add_cmd_table("T2", t1_cmd::get_t1_table());
        cli.start("");

        // Remove T1
        cli.remove_cmd_table("T1");

        // Go to T1
        io.reset();
        io.get_char_val = {'T', '1', '\n', EOF};

        cli.process();

        REQUIRE( io.print_history.size() == 4                   );
        REQUIRE( io.print_history[0]     == endline             );
        REQUIRE( io.print_history[1]     == "Command not found" );
        REQUIRE( io.print_history[2]     == endline             );
        REQUIRE( io.print_history[3]     == prompt              );
    }

    SECTION( "Remove a table that doesn't exists" ) {

        bool echo = false;
        std::string endline = "\r\n";
        std::string prompt  = ">";

        Console cli(io, cli_buffer, cli_root_table, echo, prompt.c_str(), endline.c_str());

        cli.add_cmd_table("T0", t0_cmd::get_t0_table());
        cli.add_cmd_table("T1", t1_cmd::get_t1_table());
        cli.add_cmd_table("T2", t1_cmd::get_t1_table());
        cli.start("");

        // Remove T5 which doesn't exists
        cli.remove_cmd_table("T5");

        // Go to T5
        io.reset();
        io.get_char_val = {'T', '5', '\n', EOF};

        cli.process();

        REQUIRE( io.print_history.size() == 4                   );
        REQUIRE( io.print_history[0]     == endline             );
        REQUIRE( io.print_history[1]     == "Command not found" );
        REQUIRE( io.print_history[2]     == endline             );
        REQUIRE( io.print_history[3]     == prompt              );
    }

    SECTION( "Console commands quit") {
        bool echo = false;
        std::string endline = "\r\n";
        std::string prompt  = ">";

        Console cli(io, cli_buffer, cli_root_table, echo, prompt.c_str(), endline.c_str());

        cli.add_cmd_table("T0", t0_cmd::get_t0_table());
        cli.add_cmd_table("T1", t1_cmd::get_t1_table());
        cli.start("");

        // Go to T1
        io.reset();
        io.get_char_val = {'T', '1', '\n', EOF};

        cli.process();

        // Go to root
        io.reset();
        io.get_char_val = {'q', 'u', 'i', 't', '\n', EOF};

        cli.process();

        // Call t1cmd0 command
        io.reset();
        io.get_char_val = {'t', '1', 'c', 'm', 'd', '0', '\n', EOF};

        cli.process();

        REQUIRE( io.print_history.size() == 4                   );
        REQUIRE( io.print_history[0]     == endline             );
        REQUIRE( io.print_history[1]     == "Command not found" );
        REQUIRE( io.print_history[2]     == endline             );
        REQUIRE( io.print_history[3]     == prompt              );
    }

    SECTION( "Help command when no table is selected" ) {
        bool echo = false;
        std::string endline = "\r\n";
        std::string prompt  = ">";

        Console cli(io, cli_buffer, cli_root_table, echo, prompt.c_str(), endline.c_str());

        cli.add_cmd_table("App0", t0_cmd::get_t0_table());
        cli.add_cmd_table("App1", t1_cmd::get_t1_table());
        cli.add_cmd_table("App2", t2_cmd::get_t2_table());
        cli.start("");

        io.reset();
        io.get_char_val = {'h', 'e', 'l', 'p', '\n', EOF};

        cli.process();

        // Table are printed in alphabetical order
        REQUIRE( io.print_history.size() == 8       );
        REQUIRE( io.print_history[0]     == endline );
        REQUIRE( io.print_history[1]     == "App0"  );
        REQUIRE( io.print_history[2]     == endline );
        REQUIRE( io.print_history[3]     == "App1"  );
        REQUIRE( io.print_history[4]     == endline );
        REQUIRE( io.print_history[5]     == "App2"  );
        REQUIRE( io.print_history[6]     == endline );
        REQUIRE( io.print_history[7]     == prompt  );
    }

    SECTION( "Help command when no table is selected, a table has been removed" ) {
        bool echo = false;
        std::string endline = "\r\n";
        std::string prompt  = ">";

        Console cli(io, cli_buffer, cli_root_table, echo, prompt.c_str(), endline.c_str());

        cli.add_cmd_table("App0", t0_cmd::get_t0_table());
        cli.add_cmd_table("App1", t1_cmd::get_t1_table());
        cli.add_cmd_table("App2", t2_cmd::get_t2_table());
        cli.start("");

        io.reset();
        io.get_char_val = {'h', 'e', 'l', 'p', '\n', EOF};

        cli.remove_cmd_table("App2");
        cli.process();

        // Table are printed in alphabetical order
        REQUIRE( io.print_history.size() == 6       );
        REQUIRE( io.print_history[0]     == endline );
        REQUIRE( io.print_history[1]     == "App0"  );
        REQUIRE( io.print_history[2]     == endline );
        REQUIRE( io.print_history[3]     == "App1"  );
        REQUIRE( io.print_history[4]     == endline );
        REQUIRE( io.print_history[5]     == prompt  );
    }
}

// Test helper function implementation
void run_process(Console& cli, std::uint32_t nb_time)
{
    for (std::uint32_t i = 0; i < nb_time; ++i) {
        cli.process();
    }
}
