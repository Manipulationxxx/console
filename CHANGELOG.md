# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.1.0] - 2020-10-4
### Added
- New class with console's data.
- BSD-3 license
- add_cmd_table & remove_cmd_table methods

### Changed
- Remove dynamic allocation
- Replaced makefile by CMakeList
- String is passed by reference to commands

### Removed
- Concept of Master table and Base table

## [1.0.0] - 2020-01-19

Initial release