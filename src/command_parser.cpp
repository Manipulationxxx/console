/**
 * Copyright 2020 Christian Daigneault-St-Arnaud
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions
 *    and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 *    following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

// Includes
#include "command_parser.h"
#include "console_type.h"

using namespace console;

void Parser::get_name()
{
    constexpr std::string::size_type name_begin   = 0;
    std::string::size_type  size;

    size = get_name_size();
    output = input.substr(name_begin, size);
}

std::string::size_type Parser::get_name_size()
{
    constexpr const char name_end[] = { cr_char, lf_char, parameter_start };

    return input.find_first_of(name_end);
}

void Parser::get_parameter(std::uint8_t param_number_)
{
    std::string::size_type p_begin;
    std::string::size_type p_end;

    p_begin = find_parameter_begin(param_number_);
    p_end = find_parameter_end(p_begin);

    if (is_parameter_found(p_begin)) {
        create_output_string(p_begin, p_end);
    } else {
        clear_output_string();
    }

}

std::string::size_type Parser::find_parameter_begin(std::uint8_t param_number_)
{
    std::uint8_t parameter_index = 0;
    std::string::size_type parameter_begin = 0;
    std::string::size_type search_begin = 0;

    while ((parameter_begin != std::string::npos) && (parameter_index < param_number_)) {
        parameter_begin = input.find(parameter_start, search_begin);

        if (is_parameter_found(parameter_begin)) {
            ++parameter_index;
            search_begin = parameter_begin + 1;
        }
    }
    return parameter_begin;
}

std::string::size_type Parser::find_parameter_end(std::string::size_type p_begin_)
{
    std::string::size_type first_char = p_begin_ + 1;
    std::string::size_type p_end;

    constexpr const char str_end[] = { cr_char, lf_char, parameter_start, null_char};
    p_end = input.find_first_of(str_end, first_char);

    return p_end;
}

bool Parser::is_parameter_found(std::string::size_type p_begin_)
{
    if (p_begin_ != std::string::npos) {
        return true;
    } else {
        return false;
    }
}

void Parser::create_output_string(std::string::size_type p_begin_, std::string::size_type p_end_)
{
    std::string::size_type first_char = p_begin_ + 1;
    std::string::size_type last_char  = p_end_;
    std::string::size_type length     = last_char - first_char;

    output = input.substr(first_char, length);
}

void Parser::clear_output_string()
{
    output.clear();
}
