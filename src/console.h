/**
 * Copyright 2020 Christian Daigneault-St-Arnaud
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions
 *    and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 *    following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef CONSOLE_H_
#define CONSOLE_H_

// Includes
#include <array>
#include <cstdint>
#include <map>
#include <string>
#include "command_table.h"
#include "console_io.h"
#include "console_util.h"

namespace console {

    template<std::uint32_t buffer_size, std::uint32_t root_table_size>
    class Data {
    public:
        Data()
        : rsrc_buffer(buffer_memory.data(), buffer_memory.size()),
          rsrc_root_table(root_table_memory.data(), root_table_memory.size())
        {}

        std::pmr::string& get_buffer()
        {
            return buffer;
        }

        root_table_t& get_root_table()
        {
            return root_table;
        }

    private:
        // Buffer
        static constexpr std::uint8_t str_end = 1;
        std::array<std::uint8_t, (buffer_size + str_end)> buffer_memory;
        std::pmr::monotonic_buffer_resource rsrc_buffer;
        std::pmr::string buffer {{" ", buffer_size}, &rsrc_buffer};

        // Root table
        std::array<std::uint8_t, root_table_size> root_table_memory;
        std::pmr::monotonic_buffer_resource rsrc_root_table;
        root_table_t root_table {&rsrc_root_table};
    };
}

// Class
class Console : public console::noncopyable {

public:
    Console( Console_io& console_io_,
             std::pmr::string& input_buffer_,
             root_table_t& root_table_,
             bool echo_ = true,
             const char* console_prompt_ = ">",
             const char* endline_ = "\r\n")
    : console_io(console_io_),
      commands(),
      root_table(root_table_),
      current_table(""),
      input_buffer(input_buffer_),
      echo(echo_),
      console_prompt(console_prompt_),
      endline(endline_),
      cr_eol_flag(false),
      lf_eol_flag(false)
    {}

    void start(const char* message = "");

    void process();

    void add_cmd_table(const char* name, const commands_table_t* table);

    void remove_cmd_table(const char* name);

private:
    // Private methods
    bool is_only_one_cmd_table();
    void open_table(root_table_t::iterator table);
    void reset_table();
    void get_console_input();
    bool is_command_complete();
    static bool is_delete(int ch);
    void delete_char();
    static bool is_end_of_line(int ch);
    void fill_input_buffer(int ch);
    static bool is_command_found(std::int32_t index);
    static bool is_console_command(std::int32_t index);
    void execute_command(std::int32_t index);
    void execute_console_command(std::int32_t index);
    void execute_quit();
    void execute_help();
    bool is_table_open();
    void print_command_help();
    void print_root_help();
    std::string::size_type get_max_name_length();
    void adjust_name_length(std::uint32_t index, std::string::size_type size, std::pmr::string& out);
    void print_message(const char* console_message, const char* message);
    void print_message(const char* message);
    void start_new_line();
    void set_eol_flag(int ch);
    void reset_eol_flag();
    bool is_eol_crlf_char();
    root_table_t::iterator search_root_table(const char* name);
    bool is_table_found(root_table_t::iterator table);

    // Variables
    Console_io&       console_io;
    Command_table     commands;
    root_table_t&     root_table;
    const char*       current_table;
    std::pmr::string& input_buffer;
    bool              echo;
    const char*       console_prompt;
    const char*       endline;
    bool              cr_eol_flag;
    bool              lf_eol_flag;

    // Messages
    static constexpr const char* msg_error = "Error: ";
    static constexpr const char* msg_cmd_not_found = "Command not found";
    static constexpr const char* msg_quit = "";
};


#endif /* CONSOLE_H_ */