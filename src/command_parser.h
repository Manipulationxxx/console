/**
 * Copyright 2020 Christian Daigneault-St-Arnaud
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions
 *    and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 *    following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SRC_CONSOLE_COMMAND_PARAMETER_H_
#define SRC_CONSOLE_COMMAND_PARAMETER_H_

// Includes
#include <memory_resource>
#include <string>

namespace console {

    class Parser {
    public:
        Parser(const std::pmr::string& input_, std::pmr::string& output_)
        : input(input_),
          output(output_)
        {};

        void get_name();

        void get_parameter(std::uint8_t param_number);

    private:
        std::string::size_type get_name_size();
        std::string::size_type find_parameter_begin(std::uint8_t parameter_nb);
        std::string::size_type find_parameter_end(std::string::size_type p_begin_);
        static bool is_parameter_found(std::string::size_type p_begin_);
        void create_output_string(std::string::size_type p_begin_, std::string::size_type p_end_);
        void clear_output_string();

        const std::pmr::string& input;
        std::pmr::string&       output;
    };
}


#endif /* SRC_CONSOLE_COMMAND_PARAMETER_H_ */
