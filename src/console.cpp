/**
 * Copyright 2020 Christian Daigneault-St-Arnaud
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions
 *    and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 *    following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

// Includes
#include <algorithm>
#include "command_parser.h"
#include "console.h"
#include "console_type.h"

using namespace console;

// Public methods
void Console::start(const char* message)
{
    if (is_only_one_cmd_table()) {
        open_table(root_table.begin());
    }

    print_message(message);
    start_new_line();
}

void Console::process()
{
    String_factory<max_name_length> name_str;
    std::pmr::string& name = name_str.get_string();

    Parser parser(input_buffer, name);

    std::int32_t index;

    get_console_input();

    if (is_command_complete()) {

        if (input_buffer.size() > 1) {

            parser.get_name();
            index = commands.search(input_buffer, name);

            if (is_command_found(index)) {
                execute_command(index);
            }

            if (is_console_command(index)) {
                execute_console_command(index);
            }

            if (index == not_found) {

                auto table = search_root_table(name.c_str());

                if (is_table_found(table)) {
                    open_table(table);
                } else {
                    // Not found
                    print_message(msg_cmd_not_found);
                }
            }

            start_new_line();
        } else {
            if (is_eol_crlf_char()) {
                input_buffer.clear();
            } else {
                start_new_line();
            }
        }
    }
}

void Console::add_cmd_table(const char* name, const commands_table_t* table)
{
    root_table[name] = table;
}

void Console::remove_cmd_table(const char* name)
{
    // Check if key is valid. Invalid position or key causes undefined behavior.
    auto it = search_root_table(name);

    if (is_table_found(it)) {
        root_table.erase(name);
    }
}

// Private methods
bool Console::is_only_one_cmd_table()
{
    return (root_table.size() == single_table);
}

void Console::open_table(const root_table_t::iterator table)
{
    current_table = table->first;
    commands.set_table(table->second);
}

void Console::reset_table()
{
    current_table = "";
    commands.set_table(nullptr);
}

void Console::get_console_input()
{
    char echo_out[] = {' ', '\0'};
    bool is_eol_found = false;
    int ch;

    do {
        ch = console_io.get_char();

        if (ch != EOF) {

            if (is_delete(ch)) {
                delete_char();
            } else if (is_end_of_line(ch)) {
                fill_input_buffer(ch);
                set_eol_flag(ch);
                is_eol_found = true;
            } else {
                fill_input_buffer(ch);
                if (echo) {
                    *echo_out = static_cast<char>(ch);
                    console_io.print(echo_out);
                }
            }
        }
    } while ((ch != EOF) && (!is_eol_found));
}

bool Console::is_command_complete()
{
    // string::back shall not be called on empty string.
    // Otherwise, it causes undefined behavior.
    if (input_buffer.empty()) {
        return false;
    }

    char last_input = input_buffer.back();

    if ((last_input == cr_char) || (last_input == lf_char)) {
        return true;
    } else {
        return false;
    }
}

bool Console::is_delete(const int ch)
{
    if ((ch == bs_char) || (ch == del_char)) {
        return true;
    } else {
        return false;
    }
}

void Console::delete_char()
{
    constexpr const char* delete_echo = "\b \b";

    input_buffer.pop_back();

    if (echo) {
        console_io.print(delete_echo);
    }
}

bool Console::is_end_of_line(const int ch)
{
    if ((ch == cr_char) || (ch == lf_char)) {
        return true;
    } else {
        return false;
    }
}

void Console::fill_input_buffer(const int ch)
{
    if (input_buffer.size() < input_buffer.capacity()) {
        input_buffer += static_cast<char>(ch);
    }
}

bool Console::is_command_found(const std::int32_t index)
{
    if (index > not_found) {
        return true;
    } else {
        return false;
    }
}

bool Console::is_console_command(const std::int32_t index)
{
    if (index < not_found) {
        return true;
    } else {
        return false;
    }
}

void Console::execute_command(const std::int32_t index)
{
    Cmd_result result;

    console_io.print(endline);
    result = commands.execute(index, console_io, input_buffer);

    if (result != Cmd_result::success) {
        const char* help_msg = commands.get_help_msg(index);
        print_message(msg_error, help_msg);
    }
}

void Console::execute_console_command(const std::int32_t index)
{
    switch (index) {
        case quit_found:
            execute_quit();
            break;
        case help_found:
            execute_help();
            break;
        default:
            break;
    }
}

void Console::execute_quit()
{
    reset_table();
}

void Console::execute_help()
{
    if (is_table_open()) {
        print_command_help();
    } else if (!root_table.empty()){
        print_root_help();
    }
}

bool Console::is_table_open()
{
    if (*current_table == null_char) {
        return false;
    } else {
        return true;
    }
}

void Console::print_command_help()
{
    String_factory<max_name_length> name_adj_str;
    std::pmr::string& name_adj = name_adj_str.get_string();

    std::string::size_type max_length;
    max_length = get_max_name_length();


    console_io.print(endline);
    console_io.print(current_table);

    std::uint32_t i = 0;
    while (!commands.is_table_end(i)) {
        adjust_name_length(i, max_length, name_adj);

        console_io.print(endline);
        console_io.print(commands.get_command_name(i));
        console_io.print(name_adj.c_str());
        console_io.print(" : ");
        console_io.print(commands.get_help_msg(i));
        ++i;
    }
}

void Console::print_root_help()
{
    auto read_it = root_table.begin();

    while (read_it != root_table.end()) {
        console_io.print(endline);
        console_io.print(read_it->first);
        ++read_it;
    }
}

std::string::size_type Console::get_max_name_length()
{
    std::string::size_type max_length = 0;
    std::string::size_type name_length;

    std::uint32_t i = 0;
    while (!commands.is_table_end(i)) {
        name_length = commands.get_command_name_size(i);
        max_length = std::max(max_length, name_length);
        ++i;
    }

    return max_length;
}

void Console::adjust_name_length(std::uint32_t index, std::string::size_type size, std::pmr::string& out)
{
    std::string::size_type nb_space_needed;

    nb_space_needed = size - commands.get_command_name_size(index);

    out.clear();
    out.append(nb_space_needed, ' ');
}

void Console::print_message(const char* console_message, const char* message)
{
    console_io.print(endline);
    console_io.print(console_message);
    console_io.print(message);
}

void Console::print_message(const char* message)
{
    console_io.print(endline);
    console_io.print(message);
}

void Console::start_new_line()
{
    console_io.print(endline);
    console_io.print(console_prompt);
    input_buffer.clear();
}

void Console::set_eol_flag(int ch)
{
    if (ch == cr_char) {
        cr_eol_flag = true;
    } else if (ch == lf_char) {
        lf_eol_flag = true;
    }
}

void Console::reset_eol_flag()
{
    cr_eol_flag = false;
    lf_eol_flag = false;
}

bool Console::is_eol_crlf_char()
{
    if (cr_eol_flag && lf_eol_flag) {
        reset_eol_flag();
        return true;
    } else {
        return false;
    }
}

root_table_t::iterator Console::search_root_table(const char* name)
{
    auto it = root_table.find(name);

    return it;
}

bool Console::is_table_found(root_table_t::iterator table)
{
    return (table != root_table.end());
}

