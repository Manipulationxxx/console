/**
 * Copyright 2020 Christian Daigneault-St-Arnaud
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions
 *    and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 *    following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef CONSOLE_CONSOLE_TYPE_H_
#define CONSOLE_CONSOLE_TYPE_H_

// Includes
#include <cstring>
#include <map>
#include <memory_resource>
#include <string>
#include "console_io.h"

namespace console {

    enum class Cmd_result {
        error,
        success
    };

    typedef Cmd_result (*command_t)(Console_io& io, const std::pmr::string& buffer);

    typedef struct {
        const char*        name;
        console::command_t execute;
        const char*        help;
    } commands_table_t;

    // Custom comparator for map to use const char* instead of std::string
    struct cmp_str
    {
       bool operator()(const char* a, const char* b) const
       {
          return std::strcmp(a, b) < 0;
       }
    };

    typedef std::pmr::map<const char*, const commands_table_t*, cmp_str> root_table_t;


    // Constants
    static constexpr char               cr_char          = '\n';
    static constexpr char               lf_char          = '\r';
    static constexpr char               null_char        = '\0';
    static constexpr char               bs_char          = 8;
    static constexpr char               del_char         = 127;
    static constexpr const char*        table_end        = "";
    static constexpr const char*        quit_name        = "quit";
    static constexpr const char*        help_name        = "help";
    static constexpr const std::int32_t not_found        = -1;
    static constexpr const std::int32_t quit_found       = -2;
    static constexpr const std::int32_t help_found       = -3;
    static constexpr const char         parameter_start  = ' ';
    static constexpr const std::int32_t root             = 0;
    static constexpr const std::int32_t single_table     = 1;
    static constexpr const std::int32_t max_name_length  = 24;
}


#endif /* CONSOLE_CONSOLE_TYPE_H_ */
