/**
 * Copyright 2020 Christian Daigneault-St-Arnaud
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions
 *    and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 *    following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef CONSOLE_CONSOLE_UTIL_H_
#define CONSOLE_CONSOLE_UTIL_H_

// Includes
#include <memory_resource>
#include <string>

namespace console {

    // Enable / Disable help message in table
    template<bool enable>
    constexpr const char* help(const char* help_message)
    {
        if (enable) {
            return help_message;
        } else {
            return "";
        }
    }

    // Create std::string on stack
    template <std::uint32_t size = 128>
    class String_factory {
    public:
        String_factory(): rsrc(string_memory.data(), string_memory.size()) {}

        std::pmr::string& get_string()
        {
            return str;
        }

    private:
        static constexpr std::uint8_t str_end = 1;
        std::array<std::uint8_t, size + str_end> string_memory;
        std::pmr::monotonic_buffer_resource rsrc;
        std::pmr::string str = {{" ", size}, &rsrc};
    };

    // Noncopyable
    namespace my_noncopyable_namespace
    {
        class noncopyable
        {
        protected:
          noncopyable() = default;
          ~noncopyable() = default;

        private:
          // Emphasize: The following members are private.
          noncopyable(const noncopyable&) = delete;
          noncopyable& operator=(const noncopyable&) = delete;
        };
    }

    using noncopyable = my_noncopyable_namespace::noncopyable;
}


#endif /* CONSOLE_CONSOLE_UTIL_H_ */
