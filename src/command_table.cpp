/**
 * Copyright 2020 Christian Daigneault-St-Arnaud
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions
 *    and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 *    following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

// Includes
#include "command_table.h"
#include "command_parser.h"
#include "console_util.h"

using namespace console;

std::int32_t Command_table::search(const std::pmr::string& input, std::pmr::string& name)
{
    std::int32_t index = not_found;

    Parser parser(input, name);

    parser.get_name();

    if (table != nullptr) {
        index = find_matching_name(name);
    }

    if (index == not_found) {
        index = find_matching_console_command(name);
    }

    return index;
}

std::int32_t Command_table::search(const std::pmr::string& name)
{
    std::int32_t index = not_found;

    if (table != nullptr) {
        index = find_matching_name(name);
    }

    if (index == not_found) {
        index = find_matching_console_command(name);
    }

    return index;
}

std::int32_t Command_table::find_matching_name(const std::pmr::string& name)
{
    std::int32_t i = 0;
    while (!is_table_end(i)) {
        if (is_cmd_name(name, i)) {
            return i;
        }
        ++i;
    }

    return not_found;
}

std::int32_t Command_table::find_matching_console_command(const std::pmr::string& name)
{
    std::int32_t i = not_found;

    if (name == quit_name) {
        i = quit_found;
    } else if (name == help_name) {
        i = help_found;
    }

    return i;
}

bool Command_table::is_cmd_name(const std::pmr::string& name, std::uint32_t index)
{
    return (name == get_command_name(index));
}
