/**
 * Copyright 2020 Christian Daigneault-St-Arnaud
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions
 *    and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 *    following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef CONSOLE_COMMANDS_H_
#define CONSOLE_COMMANDS_H_

// Includes
#include <cstdint>
#include <memory_resource>
#include <string>
#include "console_io.h"
#include "console_type.h"
#include "console_util.h"

using namespace console;

// Console command
class Command_table : public console::noncopyable {

public:
    Command_table( )
    : table(nullptr)
    {}

    std::int32_t search(const std::pmr::string& input);
    std::int32_t search(const std::pmr::string& input, std::pmr::string& name);

    Cmd_result execute(const std::uint32_t index, Console_io& io, const std::pmr::string& input)
    {
        return table[index].execute(io, input);
    }

    const char* get_command_name(std::int32_t index)
    {
        return table[index].name;
    }

    std::int32_t get_command_name_size(std::int32_t index)
    {
        const char* name = get_command_name(index);

        std::int32_t i = 0;
        while (name[i]) {
            ++i;
        }

        return i;
    }

    const char* get_help_msg(std::int32_t index)
    {
        return table[index].help;
    }

    void set_table(const commands_table_t* current_table)
    {
        table = current_table;
    }

    bool is_table_end(std::int32_t index)
    {
        char current = *get_command_name(index);

        return (current == *table_end);
    }

protected:

private:
    std::int32_t find_matching_name(const std::pmr::string& name);
    static std::int32_t find_matching_console_command(const std::pmr::string& name);
    bool is_cmd_name(const std::pmr::string& name, std::uint32_t index);

    // Variables
    const commands_table_t*  table;

    // Constants
    static constexpr std::string::size_type name_begin      = 0;
};


#endif /* CONSOLE_COMMANDS_H_ */
